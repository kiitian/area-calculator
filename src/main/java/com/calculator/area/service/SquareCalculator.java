package com.calculator.area.service;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SquareCalculator implements AreaCalculator {

	@Override
	public void getArea() {
		double side = -1;
		while (side < 0) {
			side = getSide();
		}
		System.out.println("Area of the sqaure: " + side * side + "\n");

	}

	private double getSide() {
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter side(s) = ");

		try {
			String input = sc.nextLine();
			if (input.contains(" ")) {
				throw new InputMismatchException();
			}
			double side = Double.parseDouble(input);
			if (side <=0 || side > 999)
				throw new InputMismatchException();
			return side;
		} catch (InputMismatchException | NumberFormatException e) {
			System.out.println("Invalid Input. 0 < side <999\n");
			return -1;
		}
	}

}
