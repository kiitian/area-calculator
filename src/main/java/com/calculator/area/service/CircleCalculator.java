package com.calculator.area.service;

import java.util.InputMismatchException;
import java.util.Scanner;

public class CircleCalculator implements AreaCalculator {

	@Override
	public void getArea() {

		double radius = -1;

		while (radius < 0) {
			radius = getRadius();
		}
		System.out.println("Area of the circle: " + (Math.PI * radius * radius) + "\n");
	}

	private double getRadius() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Radius(r) = ");

		try {
			String input = sc.nextLine();
			if (input.contains(" ")) {
				throw new InputMismatchException();
			}
			double radius = Double.parseDouble(input);
			if (radius <=0 || radius > 999)
				throw new InputMismatchException();
			return radius;
		} catch (InputMismatchException | NumberFormatException e) {
			System.out.println("Invalid Input. 0 < Radius <999\n");
			return -1;
		}

	}

}
