package com.calculator.area.service;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		while (true) {
			int option = getInput();
			if (option == 4) {
				break;
			} else if (option > 0)
				AreaCalculatorFactory.getAreaCalculator(option).getArea();
		}

		System.out.println("Thank you for choosing AreaCalculator! Have a nice day!");

	}

	private static int getInput() {
		Scanner sc = new Scanner(System.in);
		int option = -1;
		System.out.println("Select from below options: \n" + "1. Area of circle\n" + "2. Area of square\n"
				+ "3. Area of rectangle\n" + "4. Exit");
		try {
			option = sc.nextInt();
			if (option < 1 || option > 4)
				throw new InputMismatchException();
			return option;
		} catch (InputMismatchException e) {
			System.out.println("Invalid Input.\n");
			return -1;
		}
	}

}
