package com.calculator.area.service;

import java.util.InputMismatchException;
import java.util.Scanner;

public class RectangleCalculator implements AreaCalculator {

	@Override
	public void getArea() {
		double width = -1;
		double height = -1;

		while (width < 0) {
			width = getWidth();
		}
		while (height < 0) {
			height = getHeight();
		}
		System.out.println("Area of the rectangle: " + width * height + "\n");

	}

	private double getWidth() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter width(w) = ");
		try {
			String input = sc.nextLine();
			if (input.contains(" ")) {
				throw new InputMismatchException();
			}
			double width = Double.parseDouble(input);
			if (width <=0 || width > 999)
				throw new InputMismatchException();
			return width;
		} catch (InputMismatchException | NumberFormatException e) {
			System.out.println("Invalid Input. 0 < width <999\n");
			return -1;
		}

	}

	private double getHeight() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter height(h) = ");

		try {
			String input = sc.nextLine();
			if (input.contains(" ")) {
				throw new InputMismatchException();
			}
			double height = Double.parseDouble(input);
			if (height <=0 || height > 999)
				throw new InputMismatchException();
			return height;
		} catch (InputMismatchException | NumberFormatException e) {
			System.out.println("Invalid Input. 0 < height <999");
			return -1;
		}

	}
}
