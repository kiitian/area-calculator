package com.calculator.area.service;

public class AreaCalculatorFactory {

	public static AreaCalculator getAreaCalculator(int option) {
		switch (option) {
		case 1:
			return new CircleCalculator();
		case 2:
			return new SquareCalculator();
		case 3:
			return new RectangleCalculator();
		default:
			return new CircleCalculator();
		}
	}

}
